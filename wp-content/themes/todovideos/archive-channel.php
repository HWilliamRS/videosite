<?php

get_header();

while (have_posts()) {
    the_post();

?>
    <p>Vista general de canales</p>


    <div class="container-fluid">
        <div class="mx-auto tm-content-container">
            <main>
                <div class="row mb-5 pb-4">
                    <div class="col-12">

                    </div>
                </div>
                <div class="row mb-5 pb-5">
                    <div class="col-xl-8 col-lg-7">
                        <!-- Video description -->
                        <img src="" alt="" />
                        <div class="tm-video-description-box">
                            <h2 class="mb-5 tm-video-title"><?php the_title(); ?></h2>
                            <p class="mb-4"> <?php the_content(); ?></p>
                        </div>
                    </div>
                </div>
            </main>
        </div>

        
    </div>

<?php
}

wp_reset_postdata();
get_footer();


?>