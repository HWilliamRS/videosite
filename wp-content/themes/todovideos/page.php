<?php

get_header();

while(have_posts()){
    the_post();
?>

<div class="tm-page-wrap mx-auto">
		<div class="position-relative">
			
			<div class="tm-welcome-container tm-fixed-header tm-fixed-header-2">
				<div class="text-center">
					<p class="pt-5 px-3 tm-welcome-text tm-welcome-text-2 mb-1 mt-lg-0 mt-5 text-white mx-auto"><?php the_title(); ?></p>                	
				</div>                
            </div>

            <div id="tm-fixed-header-bg"></div> <!-- Header image -->
		</div>

		<!-- Page content -->
		<main>
			<div class="container-fluid px-0">
				<div class="mx-auto tm-content-container">					
					<div class="row mt-3 mb-5 pb-3">
						<div class="col-12">
							<div class="mx-auto tm-about-text-container px-3">
								<h2 class="tm-page-title mb-4 tm-text-primary">Sobre esta pagina</h2>
								<p>
                                    <?php the_content(); ?>
                                </p>
						</div>						
					</div>					
				</div>

				<div class="parallax-window" data-parallax="scroll" data-image-src="img/about-2.jpg"></div>

				

				<div class="parallax-window" data-parallax="scroll" data-image-src="img/about-3.jpg"></div>
			</div>
		</main>


	</div>

<?php

}

wp_reset_postdata();
get_footer();

?>