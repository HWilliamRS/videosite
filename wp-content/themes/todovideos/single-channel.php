<?php 

    get_header();

    while(have_posts()){
    the_post();
?>


<div class="container-fluid">
    <div class="mx-auto tm-content-container">
        <main>
            <div class="row mb-5 pb-4">
                <div class="col-12">

                </div>
            </div>
            <div class="row mb-5 pb-5">
                <div class="col-xl-8 col-lg-7">
                    <!-- Video description -->
                    <div class="tm-video-description-box">
                        <h2 class="mb-5 tm-video-title"><?php the_title(); ?></h2>
                        <p class="mb-4"> <?php the_content(); ?></p>
                    </div>
                </div>
            </div>

            
        </main>
    </div>
</div>

<div class="row tm-catalog-item-list">



            <?php
            $autores = get_field("autores");
            var_dump($autores);
            exit();

            //Related video-author
            $relatedAuthors = new WP_query(array(
                'posts_per_page' => -1,
                'post_type' => 'video-author',
                'orderby' => 'title',
                'meta_query' => array(
                    array(
                    'key' => 'canal',
                    'compare' => 'LIKE',
                    'value' => '"' . get_the_id() . '"'
                    )
                ),
                'order' => 'ASC'
            ));


            while ($relatedAuthors->have_posts()) {
                $relatedAuthors->the_post();
            ?>

                <div class="col-lg-4 col-md-6 col-sm-12 tm-catalog-item">
                    <div class="position-relative tm-thumbnail-container">
                    <?php 
                        $image = get_field('imagen');
                        
                        $size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
 
                    ?>
                        <img style="width:200px;height:200px; object-fit:cover;" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                        <a href="#" class="position-absolute tm-img-overlay">
                            <i class="fas fa-play tm-overlay-icon"></i>
                        </a>
                    </div>
                    <div class="p-4 tm-bg-gray tm-catalog-item-description">
                        <h3 class="tm-text-primary mb-3 tm-catalog-item-title"><a href="<?php the_permalink(); ?>"> <?php the_title();   ?> </a></h3>
                    </div>
                </div>

            <?php
            }
            wp_reset_postdata();
            ?>

        </div>

<?php   
    }
    get_footer();
?>