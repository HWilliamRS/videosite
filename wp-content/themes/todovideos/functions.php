<?php

function load_stylesheets(){


    //wp_enqueue_script('main-university-js',get_theme_file_uri('/js/scripts-bundled.js'),NULL,microtime(),true);    
    wp_register_style('bootstrap_stylesheet',get_stylesheet_directory_uri() . '/css/bootstrap.min.css',array(),false,"all");
    wp_register_style('fontawasome',get_stylesheet_directory_uri() . '/fontawesome/css/all.min.css',array(),false,"all");
    wp_register_style('template_stylesheet',get_stylesheet_directory_uri() . '/css/templatemo-video-catalog.css',array(),false,"all");

    wp_enqueue_style('style', get_stylesheet_uri(), NULL , microtime() );
    wp_enqueue_style('bootstrap_stylesheet');
    wp_enqueue_style('fontawasome');
    wp_enqueue_style('template_stylesheet');
    wp_enqueue_style('google_fonts','//fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap');
}

add_action("wp_enqueue_scripts", "load_stylesheets");


function aditional_features(){

    register_nav_menu('headerMenuLocation','Header Menu Location');

}

add_action("after_setup_theme","aditional_features");