<?php

get_header();

?>
<div class="tm-welcome-container text-center text-white">
    <div class="tm-welcome-container-inner">
        <p class="tm-welcome-text mb-1 text-white">Video Catalog is brought to you by TemplateMo.</p>
        <p class="tm-welcome-text mb-5 text-white">This is a full-width video banner.</p>
        <a href="#content" class="btn tm-btn-animate tm-btn-cta tm-icon-down">
            <span>Discover</span>
        </a>
    </div>
</div>

<div id="tm-video-container">
    <video autoplay muted loop id="tm-video">
        <!-- <source src="video/sunset-timelapse-video.mp4" type="video/mp4"> -->
        <source src="<?php echo get_theme_file_uri("/videos/wheat-field.mp4"); ?>" type="video/mp4">
    </video>
</div>

<i id="tm-video-control-button" class="fas fa-pause"></i>
</div>

<div class="container-fluid">
    <div id="content" class="mx-auto tm-content-container">
        <main>
            <div class="row">
                <div class="col-12">
                    <h2 class="tm-page-title mb-4">Our Video Catalog</h2>
                    <div class="tm-categories-container mb-5">
                        <h3 class="tm-text-primary tm-categories-text">Categories:</h3>
                        <ul class="nav tm-category-list">
                            <li class="nav-item tm-category-item"><a href="#" class="nav-link tm-category-link active">All</a></li>
                            <li class="nav-item tm-category-item"><a href="#" class="nav-link tm-category-link">Drone Shots</a></li>
                            <li class="nav-item tm-category-item"><a href="#" class="nav-link tm-category-link">Nature</a></li>
                            <li class="nav-item tm-category-item"><a href="#" class="nav-link tm-category-link">Actions</a></li>
                            <li class="nav-item tm-category-item"><a href="#" class="nav-link tm-category-link">Featured</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row tm-catalog-item-list">

                <?php

                while (have_posts()) {
                    the_post();

                ?>

                    <div class="col-lg-4 col-md-6 col-sm-12 tm-catalog-item">
                        <div class="position-relative tm-thumbnail-container">
                            <img src="<?php echo get_theme_file_uri('img/tn-01.jpg'); ?>" alt="Image" class="img-fluid tm-catalog-item-img">
                            <a href="video-page.html" class="position-absolute tm-img-overlay">
                                <i class="fas fa-play tm-overlay-icon"></i>
                            </a>
                        </div>
                        <div class="p-4 tm-bg-gray tm-catalog-item-description">
                            <h3 class="tm-text-primary mb-3 tm-catalog-item-title"><a href="<?php the_permalink(); ?>"> <?php the_title();   ?> </a></h3>
                            <p class="tm-catalog-item-text">
                                <?php echo wp_trim_words(get_the_content(), 12); ?>

                            </p>
                            <a href="<?php the_permalink(); ?>" class="nu gray">Leer mas...</a>
                        </div>
                    </div>

                <?php
                }
                wp_reset_postdata();
                ?>

            </div>

            <!-- Catalog Paging Buttons -->
            <div>
                <ul class="nav tm-paging-links">
                    <li class="nav-item active"><a href="#" class="nav-link tm-paging-link">1</a></li>
                    <li class="nav-item"><a href="#" class="nav-link tm-paging-link">2</a></li>
                    <li class="nav-item"><a href="#" class="nav-link tm-paging-link">3</a></li>
                    <li class="nav-item"><a href="#" class="nav-link tm-paging-link">4</a></li>
                    <li class="nav-item"><a href="#" class="nav-link tm-paging-link">></a></li>
                </ul>
            </div>
        </main>

        <!-- Subscribe form and footer links -->
        <div class="row mt-5 pt-3">
            <div class="col-xl-6 col-lg-12 mb-4">
                <div class="tm-bg-gray p-5 h-100">
                    <h3 class="tm-text-primary mb-3">Do you want to get our latest updates?</h3>
                    <p class="mb-5">Please subscribe our newsletter for upcoming new videos and latest information about our
                        work. Thank you.</p>
                    <form action="" method="GET" class="tm-subscribe-form">
                        <input type="text" name="email" placeholder="Your Email..." required>
                        <button type="submit" class="btn rounded-0 btn-primary tm-btn-small">Subscribe</button>
                    </form>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 mb-4">
                <div class="p-5 tm-bg-gray">
                    <h3 class="tm-text-primary mb-4">Quick Links</h3>
                    <ul class="list-unstyled tm-footer-links">
                        <li><a href="#">Duis bibendum</a></li>
                        <li><a href="#">Purus non dignissim</a></li>
                        <li><a href="#">Sapien metus gravida</a></li>
                        <li><a href="#">Eget consequat</a></li>
                        <li><a href="#">Praesent eu pulvinar</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 mb-4">
                <div class="p-5 tm-bg-gray h-100">
                    <h3 class="tm-text-primary mb-4">Our Pages</h3>
                    <ul class="list-unstyled tm-footer-links">
                        <li><a href="#">Our Videos</a></li>
                        <li><a href="#">License Terms</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Privacy Policies</a></li>
                    </ul>
                </div>
            </div>
        </div> <!-- row -->
    </div> <!-- tm-content-container -->
</div>

<?php
get_footer();
?>