<?php

function videosite_custom_post_types()
{
        register_post_type(
            'channel',
            array(
                'supports' => array('title', 'editor'),
                'rewrite' => array(
                    'slug' => 'channels'
                ),
                'labels' => array(
                    'name'          => __('Canales', 'textdomain'),
                    'singular_name' => __('Canal', 'textdomain'),
                ),
                'public'      => true,
                'has_archive' => true,
                'menu_icon' => 'dashicons-images-alt'
            )
        );

        register_post_type(
            'video-author',
            array(
                'supports' => array('title', 'editor', 'thumbnail'),
                'rewrite' => array(
                    'slug' => 'videos-authors'
                ),
                'labels' => array(
                    'name'          => __('Autores de videos', 'textdomain'),
                    'singular_name' => __('Autor de video', 'textdomain'),
                ),
                'public'      => true,
                'has_archive' => true,
                'menu_icon' => 'dashicons-businessperson'
            )
        );
    
        register_post_type(
            'video',
            array(
                'supports' => array('title', 'editor', 'thumbnail'),
                'rewrite' => array(
                    'slug' => 'videos'
                ),
                'labels' => array(
                    'name'          => __('Videos', 'textdomain'),
                    'singular_name' => __('Video', 'textdomain'),
                ),
                'public'      => true,
                'has_archive' => true,
                'menu_icon' => 'dashicons-video-alt3'
            )
        );

}


add_action('init','videosite_custom_post_types');